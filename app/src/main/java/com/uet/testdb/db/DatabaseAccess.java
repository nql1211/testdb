package com.uet.testdb.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.uet.testdb.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess  {
    private SQLiteOpenHelper sqLiteOpenHelper;
    private SQLiteDatabase database;
    private static  DatabaseAccess instance;

    public DatabaseAccess(Context context){
        this.sqLiteOpenHelper = new DatabaseOpenHelper(context);
    }

    public static DatabaseAccess getInstance(Context context){
        if(instance == null){
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void open(){
        this.database = sqLiteOpenHelper.getWritableDatabase();
    }

    public void close(){
        if(database !=null){
            this.database.close();
        }
    }

    public void insertContact(Contact contact) {
        ContentValues values = new ContentValues();
        values.put("first_name", contact.getFirstName());
        values.put("last_name", contact.getLastName());
        values.put("phone", contact.getPhone());
        values.put("email", contact.getEmail());
        database.insert("Contact", null, values);
    }

    public List<Contact> getContacts() {
        List<Contact> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM Contact", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Contact contact = new Contact();
            contact.setFirstName(cursor.getString(0));
            contact.setLastName(cursor.getString(1));
            contact.setPhone(cursor.getString(2));
            contact.setEmail(cursor.getString(3));
            list.add(contact);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public void updateContact(Contact oldContact, Contact newContact) {
        ContentValues values = new ContentValues();
        values.put("first_name", newContact.getFirstName());
        values.put("last_name", newContact.getLastName());
        values.put("phone", newContact.getPhone());
        values.put("email", newContact.getEmail());
        database.update("Contact", values, "phone = ?", new String[]{oldContact.getPhone()});
    }

    public void deleteContact(Contact contact) {
        database.delete("Contact", "phone = ?", new String[]{contact.getPhone()});
        database.close();
    }
}
